import java.util.Scanner;

public class Menu {
	private School school;
	
	public Menu() {
		school = new School();
	}
	
	public void runMenu() {
		boolean run = false;
		Scanner scan = new Scanner(System.in);
		
		do{
			int input = printChoises(scan);
		
			switch(input) {
			case 1:
				String name = "erik";
				Person newPerson = new Student(name, 1988);
				school.addPerson(newPerson);
				break;
			case 2:
				
				break;
			case 3:
				school.printStudents();
				break;
			case 4:
				school.printTeachers();
				break;
			default:
				run = false;
			}
			
		} while(run);
	}

	private int printChoises(Scanner scan) {
		System.out.println("Hi welcome!");
		System.out.println("1. Add student");
		System.out.println("2. Add teacher");
		System.out.println("3. Print student");
		System.out.println("4. Print teacher");
		
		int input = scan.nextInt();
		return input;
	}
	
}
