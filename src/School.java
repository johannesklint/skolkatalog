import java.util.ArrayList;

public class School {
	private ArrayList<Teacher> teachers;
	private ArrayList<Student> students;
	private ArrayList<Program> programs;
	
	public School() {
		teachers = new ArrayList<Teacher>();
		students = new ArrayList<Student>();
		programs = new ArrayList<Program>();
	}
	
	public void addPerson(Person person) {
		if (person instanceof Student) {
			students.add((Student) person);
		} else {
			teachers.add((Teacher) person);
		}
	}

	public void printStudents() {
		printPeople(students);
	}
	
	public void printTeachers() {
		printPeople(teachers);	
	}
	
	private <T extends Person> void printPeople(ArrayList<T> people) {
		for (Person person : people) {
			System.out.println(person.toString());
		}
	}
	
	public void addStudentToProgram(Student student, Program program) {
		// to-do make fix
	}
}
