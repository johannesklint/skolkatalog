
public abstract class Person {
	
	private Address address;
	private String name;
	private int birthyear;

	public Person(String name, int birthyear) {
		
		this(Address.getDefault() ,name, birthyear);
	}
	
	public Person(Address address, String name, int birthyear) {
		this.address = address;
		this.name = name;
		this.birthyear = birthyear;
	}

	@Override
	public String toString() {
		return name + ", year: " + birthyear + ", address: " + address.get();
	}
	
	
}
